import cv2 as cv

face_cascade = cv.CascadeClassifier('cars.xml')
vc = cv.VideoCapture('Test20.mp4')
 
if vc.isOpened():
    rval , frame = vc.read()
else:
    rval = False
 
while rval:
    rval, frame = vc.read()
 
    # car detection.
    cars = face_cascade.detectMultiScale(frame, 1.1, 2)
 
    ncars = 0
    for (x,y,w,h) in cars:
        cv.rectangle(frame,(x,y),(x+w,y+h),(0,0,255),2)
        ncars = ncars + 1

    font = cv.FONT_HERSHEY_SIMPLEX
    cv.putText(frame,'cars : '+str(ncars),(50,100), font, 1,(0,0,0),2,cv.LINE_AA)
    # show result
    # print(ncars)
    cv.imshow("Result",frame)
    cv.waitKey(1);
vc.release()