from imageai.Detection import ObjectDetection
import os
from PIL import Image

count = 0
execution_path = os.getcwd()

detector = ObjectDetection()
detector.setModelTypeAsYOLOv3()
detector.setModelPath( os.path.join(execution_path , "yolo.h5"))
detector.loadModel()

detections = detector.detectObjectsFromImage(input_image=os.path.join(execution_path , "Imagetest.jpg"), output_image_path=os.path.join(execution_path , "imagetest.jpg"))

for eachObject in detections:
        if eachObject["name"] == "car" or eachObject["name"] == "bus" or eachObject["name"] == "truck" :
            count = count+1
    
        print(eachObject["name"] , " : " , eachObject["percentage_probability"] )
print(count)

image = Image.open('Imagetest.jpg')
image.show()