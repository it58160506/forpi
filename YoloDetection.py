from imageai.Detection import VideoObjectDetection
import os
from matplotlib import pyplot as plt
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

execution_path = os.getcwd()

resized = False

def forFrame(frame_number, output_array, output_count, returned_frame):

    plt.clf()
    labels = []
    counter = 0

    for eachItem in output_count:
        counter += 1
        labels.append(str(output_count[eachItem]))

    global resized

    if (resized == False):
        manager = plt.get_current_fig_manager()
        manager.resize(width=1000, height=500)
        resized = True

    plt.axis("off")
    plt.imshow(returned_frame, interpolation="none")

    plt.title("Count: " + str(labels))

    plt.pause(0.01)

video_detector = VideoObjectDetection()
video_detector.setModelTypeAsYOLOv3()
video_detector.setModelPath(os.path.join(execution_path, "yolo.h5"))
video_detector.loadModel(detection_speed="flash")

plt.show()
custom_objects = video_detector.CustomObjects(car=True)
video_detector.detectCustomObjectsFromVideo(custom_objects=custom_objects, 
input_file_path=os.path.join(execution_path, "Test20.mp4"), 
output_file_path=os.path.join(execution_path, "Output1") , 
frames_per_second=5, per_frame_function=forFrame,  
minimum_percentage_probability=30, return_detected_frame=True)